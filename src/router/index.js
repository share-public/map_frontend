import { createRouter, createWebHistory } from 'vue-router'
import QueryMapView from '../views/QueryMap/QueryMapView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: QueryMapView
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
