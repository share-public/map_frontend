import { createStore } from 'vuex'

export default createStore({
  state: {
    baseURL: "http://127.0.0.1:8000/api/", // URL หลักที่ใช้ Call ไปหา back-end
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
