import axioss from 'axios';

axioss.interceptors.request.use(function (config) { // แนบค่า basic authentication ก่อนยิง request ไป server
    config.auth = {
        username: 'query_map',
        password: 'cXVlcnlfbWFw'
    };
    return config;
});

axioss.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    return Promise.reject(error);
});

export const AxiossCustom = axioss;

